def set_user
  before { session[:user_id] = Fabricate(:user).id }
end

def set_admin
  before { session[:user_id] = Fabricate(:admin).id }
end

def log_in(a_user=nil)
  user = a_user || Fabricate(:user)
  visit login_path
  fill_in "username", with: user.username
  fill_in "password", with: user.password
  click_button "Login"
end

def log_out
  visit logout_path
end

def clear_user
  session[:user_id] = nil
end

def current_user
  @current_user ||= User.find(session[:user_id]) if session[:user_id]
end