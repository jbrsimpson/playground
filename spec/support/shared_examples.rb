shared_examples "require_user" do
  it "redirects to the login page" do
    clear_user
    action
    expect(response).to redirect_to login_path
  end
end

shared_examples "require_creator" do
  it "redirects to the problem path" do
    session[:user_id] = Fabricate(:user).id
    action
    expect(response).to redirect_to problem_path(assigns(:problem))
  end
end

shared_examples "require_right_user" do
  it "redirects to problems path" do
    session[:user_id] = Fabricate(:user).id
    action
    expect(response).to redirect_to problems_path
  end
end

shared_examples "require_admin" do
  it "redirects to the login page" do
    session[:user_id] = nil
    action
    expect(response).to redirect_to login_path
  end
end

shared_examples "has_a_slug" do
  it "generates a slug before create" do  
    expect(model.slug).to be_present
  end

  it "returns the slug when to_param is called" do
    expect(model.to_param).to eq(model.slug)
  end
end