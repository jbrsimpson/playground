Fabricator(:user) do
  username { Faker::Internet.user_name }
  email { Faker::Internet.email }
  password { Faker::Internet.password}
end

Fabricator(:admin, from: :user) do
  role { "admin" }
end

Fabricator(:problem) do
  user { Fabricate(:user) }
  category { Fabricate(:category) }
  title { Faker::Company.catch_phrase }
  description { "A problem that is bugging me" }
  error_message { "404: error not found" }
  solution { "The solution, which turned out to be very simple, but was not restarting" }
end

Fabricator(:category) do
  name { Faker::Company.name }
  parent { nil}
end