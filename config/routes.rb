Rails.application.routes.draw do
  root to: "pages#home"

  devise_for :users
  resources :users, only: [:index, :show, :destroy]

  if Rails.env.development?
    mount LetterOpenerWeb::Engine, at: "/emails"
    get "/test", to: "pages#test"
  end
end
