require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module Playground
  class Application < Rails::Application
    config.active_record.raise_in_transactional_callbacks = true


    config.autoload_paths += %W(#{config.root}/lib)
    config.time_zone = 'Pacific Time (US & Canada)'
  end
end
