class ApplicationMailer < ActionMailer::Base
  default from: "ben@playground.ben"
  layout 'mailer'

  def receipt(payment)
    @payment = payment
    mail(to: @payment.email, subject: "Thanks for your purchase")
  end

  def new_payment(payment)
    @payment = payment
    mail(subject: "New purchase at Backyard Spruce")
  end

  def new_comment(comment)
    @comment = comment
    mail(subject: "New comment at Backyard Spruce")
  end
end
